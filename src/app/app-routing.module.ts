import { HistoryPage } from './shared/components/history/history.page';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AcronymsPage } from './shared/components/acronyms/acronyms.page';
import { CompanyPage } from './shared/components/company/company.page';
import { TwitterPage } from './shared/components/twitter/twitter.page';


const routes: Routes = [
  {
    path: 'launches',
    loadChildren: () => import('./launches/launches.module').then( m => m.LaunchesPageModule)
  },
  {
    path: 'cores',
    loadChildren: () => import('./cores/cores.module').then( m => m.CoresPageModule)
  },
  {
    path: 'vehicles',
    loadChildren: () => import('./vihecles/vihecles.module').then( m => m.ViheclesPageModule)
  },
  {
    path: '', redirectTo: 'launches', pathMatch: 'full'
  },
  { path: 'twitter', component: TwitterPage },
  { path: 'company', component: CompanyPage },
  { path: 'acronyms', component: AcronymsPage },
  { path: 'history', component: HistoryPage },
  // { path: '**', component: NotFoundPage }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
