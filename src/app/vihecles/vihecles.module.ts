import { SingleRocketPage } from './single-rocket/single-rocket.page';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViheclesPage } from './vihecles.page';

import { ViheclesPageRoutingModule } from './vihecles-routing.module';
import { SingleCapsulePage } from './single-capsule/single-capsule.page';
import { CapsulesPage } from './capsules/capsules.page';
import { RocketsPage } from './rockets/rockets.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ViheclesPageRoutingModule
  ],
  declarations: [ViheclesPage, SingleRocketPage, SingleCapsulePage, CapsulesPage, RocketsPage]
})
export class ViheclesPageModule {}
