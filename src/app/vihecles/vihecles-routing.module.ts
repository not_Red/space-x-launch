import { SingleRocketPage } from './single-rocket/single-rocket.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViheclesPage } from './vihecles.page';
import { SingleCapsulePage } from './single-capsule/single-capsule.page';

const routes: Routes = [
  {
    path: '',
    component: ViheclesPage,
  },
  {
    path: 'rockets/:id',
    component: SingleRocketPage,
  },
  {
    path: 'capsules/:id',
    component: SingleCapsulePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViheclesPageRoutingModule {}
