import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehiclesService } from '../core/services/spaxe-c-vehicles.service';

@Component({
  selector: 'app-vihecles',
  templateUrl: 'vihecles.page.html',
  styleUrls: ['vihecles.page.css']
})
export class ViheclesPage implements OnInit {

  rockets = true;
  public show = false;
  constructor(

  ) {}

  ngOnInit() {

  }

  filterList() {}

  loadRockets() {
    this.rockets = true;
  }
  loadCapsules() {
    this.rockets = false;
  }

  sort() {}
  toggle() {
    this.show = !this.show;
  }
}
