import { RocketDTO } from './../../shared/models/rocket-dto';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehiclesService } from 'src/app/core/services/spaxe-c-vehicles.service';

@Component({
  selector: 'app-rockets',
  templateUrl: 'rockets.page.html',
  styleUrls: ['rockets.page.css']
})
export class RocketsPage implements OnInit {

  rockets: RocketDTO[];
  constructor(
    private readonly vehiclesService: VehiclesService,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.vehiclesService.getAllRockets().subscribe((data) => {
      this.rockets = data;
    });
  }

  rocketPage(rocket: RocketDTO): void {
    this.router.navigate(['/vehicles/rockets', rocket.rocket_id]);
  }

}
