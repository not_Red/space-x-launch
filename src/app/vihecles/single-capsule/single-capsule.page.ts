import { CapsuleDTO } from './../../shared/models/capsule-dto';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { VehiclesService } from 'src/app/core/services/spaxe-c-vehicles.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-single-capsule',
  templateUrl: 'single-capsule.page.html',
  styleUrls: ['single-capsule.page.css']
})
export class SingleCapsulePage implements OnInit {
  capsuleId: string;
  capsule: CapsuleDTO;

  constructor(
    private readonly vehiclesService: VehiclesService,
    private readonly route: ActivatedRoute,
    private readonly toastController: ToastController,
    private readonly location: Location,
  ) {}
  ngOnInit() {
    this.capsuleId = this.route.snapshot.paramMap.get('id');
    this.vehiclesService.getSingleCapsule(this.capsuleId).subscribe((data) => {
      this.capsule = data;
    });
  }
  goBack() {
    this.location.back();
  }

  async toastTest() {
    const toast = await this.toastController.create({
      message: 'Work in progress!',
      duration: 2000,
    });
    toast.present();
  }
}
