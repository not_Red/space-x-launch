import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehiclesService } from 'src/app/core/services/spaxe-c-vehicles.service';
import { CapsuleDTO } from 'src/app/shared/models/capsule-dto';

@Component({
  selector: 'app-capsules',
  templateUrl: 'capsules.page.html',
  styleUrls: ['capsules.page.css']
})
export class CapsulesPage implements OnInit {

  capsules: CapsuleDTO[];
  constructor(
    private readonly vehiclesService: VehiclesService,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.vehiclesService.getAllCapsules().subscribe((data) => {
      this.capsules = data;
    });
  }

  capsulePage(capsule: CapsuleDTO): void {
    this.router.navigate(['/vehicles/capsules', capsule.capsule_serial]);
  }
}
