import { VehiclesService } from 'src/app/core/services/spaxe-c-vehicles.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Location } from '@angular/common';
import { RocketDTO } from 'src/app/shared/models/rocket-dto';


@Component({
  selector: 'app-single-rocket',
  templateUrl: 'single-rocket.page.html',
  styleUrls: ['single-rocket.page.css']
})
export class SingleRocketPage implements OnInit {
 rocketId: string;
 rocket: RocketDTO;
  constructor(
    private readonly vehiclesService: VehiclesService,
    private readonly route: ActivatedRoute,
    private readonly toastController: ToastController,
    private readonly location: Location,
  ) {}
  ngOnInit() {
    this.rocketId = this.route.snapshot.paramMap.get('id');
    this.vehiclesService.getSingleRocket(this.rocketId).subscribe((data) => {
      this.rocket = data;
    });
  }

  goBack() {
    this.location.back();
  }

  async toastTest() {
    const toast = await this.toastController.create({
      message: 'Work in progress!',
      duration: 2000,
    });
    toast.present();
  }
}
