import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

declare var twttr: any;

@Component({

  selector: 'app-twitter',
  templateUrl: 'twitter.page.html',
  styleUrls: ['twitter.page.css']
})

export class TwitterPage implements AfterViewInit {
  status = true;
  constructor(
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngAfterViewInit(): void {
    twttr.widgets.load();
  }

  refresh() {
    this.document.location.reload();
  }
}
