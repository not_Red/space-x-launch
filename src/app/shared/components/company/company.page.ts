import { InfoDTO } from './../../models/info-dto';
import {
    Component,
    OnInit
} from '@angular/core';
import { InfoService } from 'src/app/core/services/space-x-info.service';

@Component({
    selector: 'app-company',
    templateUrl: 'company.page.html',
    styleUrls: ['company.page.css']
})
export class CompanyPage implements OnInit {
    info: InfoDTO;
    constructor(
        private readonly infoService: InfoService,
    ) {}

    ngOnInit() {
        this.infoService.getInfo().subscribe((data) => {
            this.info = data;
        });
    }
}
