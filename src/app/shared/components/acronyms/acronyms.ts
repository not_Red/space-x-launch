import { AcronymDTO } from '../../models/acronym-dto';

export const allAcronyms: AcronymDTO[] = [{
        acronym: 'ACES',
        fullText: 'Advanced Cryogenic Evolved Stage',
        details: 'A LOX/LH2 upper stage being developed for ULA\'s Vulcan launch vehicle'
    },
    {
        acronym: 'ACS',
        fullText: 'Attitude Control System',
        details: 'Synonymous with Reaction Control System'
    },
    {
        acronym: 'AOS',
        fullText: 'Acquisition of Singal',
        details: 'A phrase used during launch when communication with the vehicle is re-established after Loss of Signal'
    },
    {
        acronym: 'ARM',
        fullText: 'Asteroid Redirect Mission',
        details: 'A planned NASA mission to redirect an asteroid to lunar orbit for rendezvous with an Orion spacecraft'
    },
    {
        acronym: 'ASDS',
        fullText: 'Autonomous Spaceport Drone Ship',
        details: 'SpaceX\'s custom-build landing barge'
    },
    {
        acronym: 'ASOG',
        fullText: 'A Shortfall of Gravitas',
        details: 'Moniker given to an upcoming Atlantic ASDS'
    },
    {
        acronym: 'AVI',
        fullText: 'Avionics Operator',
        details: 'Avionic Operator'
    },
    {
        acronym: 'BEAM',
        fullText: 'Bigelow Expandable Activities Module',
        details: 'An inflatable module designed by Bigelow Aerospace that will be brought up to the ISS aboard CRS-8'
    },
    {
        acronym: 'BEO',
        fullText: 'Beyond Earth Orbit',
        details: 'Any location or orbit outside of the Earth\s gravitational influence'
    },
    {
        acronym: 'BFR',
        fullText: 'Big Falcon Rocket',
        details: 'A generic term for the next SpaceX rocket, which will be larger than Falcon Heavy'
    },
    {
        acronym: 'BO',
        fullText: 'Blue Origin',
        details: 'A competitor of SpaceX'
    },
    {
        acronym: 'C3',
        fullText: 'Characteristic Energy',
        details: 'A measure of escape velocity (< 1 = no escape, 1 = escape with no further velocity, > 1 = escape with increasing velocity)'
    },
    {
        acronym: 'CBM',
        fullText: 'Common Berthing Mechanism',
        details: 'A device used to attach the cargo Dragon to the ISS'
    },
    {
        acronym: 'CC',
        fullText: 'Communications Coordinator',
        details: 'Communications Coordinator'
    },
    {
        acronym: 'CCAFS',
        fullText: 'Cape Canaveral Air Force Station',
        details: 'Cape Canaveral Air Force Station'
    },
    {
        acronym: 'CCDev',
        fullText: 'Commercial Crew Development',
        details: 'Commercial Crew Development'
    },
    {
        acronym: 'CCiCap',
        fullText: 'Commercial Crew Integrated Capability',
        details: 'Commercial Crew Integrated Capability'
    },
    {
        acronym: 'CCtCap',
        fullText: 'Commercial Crew Transopration Capability',
        details: 'Commercial Crew Transopration Capability'
    },
    {
        acronym: 'CE',
        fullText: 'Chief Engineer',
        details: 'Chief Engineer'
    },
    {
        acronym: 'CH4',
        fullText: 'Methane',
        details: 'A simply hydrocarbon used as a cryogenic fuel'
    },
    {
        acronym: 'COPV',
        fullText: 'Composite Overwrapped Pressure Vessel',
        details: 'A pressure tank constructed from a thin, non-structural liner wrapped with structural fiber composite'
    },
    {
        acronym: 'COTS',
        fullText: 'Commercial Orbital Transportation Services',
        details: 'Commercial Orbital Transportation Services'
    },
    {
        acronym: 'CRS',
        fullText: 'Commercial Resupply Services',
        details: 'A contract that SpaceX and Orbital both have with NASA to resupply the ISS'
    },
    {
        acronym: 'DC',
        fullText: 'Dragon Controller',
        details: 'Dragon Controller'
    },
    {
        acronym: 'Δv',
        fullText: '(Delta-v) Change in velocity',
        details: 'The amount of \'efort\' required to move a spacecraft from one location or orbit to another'
    },
    {
        acronym: 'DPL',
        fullText: 'Donwrange Propulsive Landing',
        details: 'Donwrange Propulsive Landing (on an ocean barge/ASDS)'
    },
    {
        acronym: 'DRO',
        fullText: 'Distant Retrogade Orbit',
        details: 'A highly stable linar orbit, proposed as the destination for the ARM asteroid'
    },
    {
        acronym: 'EAR',
        fullText: 'Export Administration Regulations',
        details: 'A US law that regulates the export of technology with \'dual-use\' applications (both commercial and military)'
    },
    {
        acronym: 'EDL',
        fullText: 'Entry, Descent and Landing',
        details: 'The series of controlled manoeuvres and pathways used to land an orbiting spacecraft'
    },
    {
        acronym: 'F1',
        fullText: 'Falcon 1',
        details: 'The Falcon 1 was an expendable launch system privately developed and manufactured by SpaceX during 2006-2009'
    },
    {
        acronym: 'F9',
        fullText: 'Falcon 9',
        details: 'The Falcon 9 is a family of two-staged-to-orbit medium lift launch vehicles, named for its use of nine Merlin first-stage engines, designed and manufactured by SpaceX'
    },
    {
        acronym: 'FFR',
        fullText: 'Flight Readiness Review',
        details: 'A paperwork exercise used to ensure that a rocket is ready to fly'
    },
    {
        acronym: 'FH',
        fullText: 'Falcon Heavy',
        details: 'The Falcon Heavy is a partially reusable super heavy-lift launch vehicle designed and manufactired by SpaceX'
    },
    {
        acronym: 'FRC',
        fullText: 'Falcon Recovery Coordinator',
        details: 'Falcon Recovery Coordinator'
    },
    {
        acronym: 'FRR',
        fullText: 'Falcon Readiness Review',
        details: 'Falcon Readiness Review'
    },
    {
        acronym: 'FS',
        fullText: 'Flight Software',
        details: 'Flight Software'
    },
    {
        acronym: 'FSPO',
        fullText: 'Flight Safety Progect Officer',
        details: 'Flight Safety Progect Officer'
    },
    {
        acronym: 'FSS',
        fullText: 'Fixed Service Structure',
        details: 'A structure on the pad at Launch Complex 39A, Cape Canaveral'
    },
    {
        acronym: 'FT',
        fullText: 'Full Thrust',
        details: 'Full Thrust'
    },
    {
        acronym: 'FTS',
        fullText: 'Flight Termination System',
        details: 'Flight Termination System'
    },
    {
        acronym: 'GS',
        fullText: 'Ground Control',
        details: 'Ground Control'
    },
    {
        acronym: 'GEO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'GNC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'GNC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'GS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'GSE',
        fullText: '',
        details: ''
    },
    {
        acronym: 'GTO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'HEO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'HIF',
        fullText: '',
        details: ''
    },
    {
        acronym: 'IAC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'IIP',
        fullText: '',
        details: ''
    },
    {
        acronym: 'IMMT',
        fullText: '',
        details: ''
    },
    {
        acronym: 'Isp',
        fullText: '',
        details: ''
    },
    {
        acronym: 'ISRU',
        fullText: '',
        details: ''
    },
    {
        acronym: 'ISS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'ITAR',
        fullText: '',
        details: ''
    },
    {
        acronym: 'GRTI',
        fullText: '',
        details: ''
    },
    {
        acronym: 'JWST',
        fullText: '',
        details: ''
    },
    {
        acronym: 'KSC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'L1',
        fullText: '',
        details: ''
    },
    {
        acronym: 'L2',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LAS/LES',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LS-13',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LC-39A',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LD',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LDA',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LEO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LH2',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LNG',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LOI',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LOM',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LOS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LOV',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LOX',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LRR',
        fullText: '',
        details: ''
    },
    {
        acronym: 'LZ-1',
        fullText: '',
        details: ''
    },
    {
        acronym: 'M1A',
        fullText: '',
        details: ''
    },
    {
        acronym: 'M1B',
        fullText: '',
        details: ''
    },
    {
        acronym: 'M1C',
        fullText: '',
        details: ''
    },
    {
        acronym: 'M1D',
        fullText: '',
        details: ''
    },
    {
        acronym: 'M1DVac',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MA',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MARS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MCT',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MECO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MEO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MM',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MMH',
        fullText: '',
        details: ''
    },
    {
        acronym: 'MOI',
        fullText: '',
        details: ''
    },
    {
        acronym: 'N2O4',
        fullText: '',
        details: ''
    },
    {
        acronym: 'NDA',
        fullText: '',
        details: ''
    },
    {
        acronym: 'NDS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'NEO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'NET',
        fullText: '',
        details: ''
    },
    {
        acronym: 'NSF',
        fullText: '',
        details: ''
    },
    {
        acronym: 'OCISLY',
        fullText: '',
        details: ''
    },
    {
        acronym: 'OD',
        fullText: '',
        details: ''
    },
    {
        acronym: 'OSM',
        fullText: '',
        details: ''
    },
    {
        acronym: 'PICA-X',
        fullText: '',
        details: ''
    },
    {
        acronym: 'PIF',
        fullText: '',
        details: ''
    },
    {
        acronym: 'PMA',
        fullText: '',
        details: ''
    },
    {
        acronym: 'PMF',
        fullText: '',
        details: ''
    },
    {
        acronym: 'Prop',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RCO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RCS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'Recovery',
        fullText: '',
        details: ''
    },
            {
        acronym: 'ROC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RP-1',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RSS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RTLS',
        fullText: '',
        details: ''
    },
    {
        acronym: 'RUD',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SE',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SECO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SEP',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SF',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SLC-40',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SLC-4E',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SLC-4W',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SPIF',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SSME',
        fullText: '',
        details: ''
    },
    {
        acronym: 'SSTO',
        fullText: '',
        details: ''
    },
    {
        acronym: 'T/E',
        fullText: '',
        details: ''
    },
    {
        acronym: 'TEL',
        fullText: '',
        details: ''
    },
    {
        acronym: 'TLI',
        fullText: '',
        details: ''
    },
    {
        acronym: 'TMI',
        fullText: '',
        details: ''
    },
    {
        acronym: 'TRL',
        fullText: '',
        details: ''
    },
    {
        acronym: 'TVC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'ULA',
        fullText: '',
        details: ''
    },
    {
        acronym: 'VAB',
        fullText: '',
        details: ''
    },
            {
        acronym: 'VAFB',
        fullText: '',
        details: ''
    },
    {
        acronym: 'VC',
        fullText: '',
        details: ''
    },
    {
        acronym: 'VV',
        fullText: '',
        details: ''
    },
    {
        acronym: 'WDR',
        fullText: '',
        details: ''
    },
];
