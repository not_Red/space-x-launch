import {
    AcronymDTO
} from './../../models/acronym-dto';
import {
    Component, OnInit
} from '@angular/core';
import { allAcronyms } from './acronyms';

@Component({
    selector: 'app-acronyms',
    templateUrl: 'acronyms.page.html',
    styleUrls: ['acronyms.page.css']
})
export class AcronymsPage implements OnInit {

    acronyms: AcronymDTO[];
    public show = false;
    constructor(

    ) {}

    ngOnInit() {
        this.acronyms = allAcronyms;
    }

    filterList(evt) {
        this.acronyms = allAcronyms;
        const searchTerm = evt.srcElement.value;

        if (!searchTerm) {
            return;
        }

        this.acronyms = this.acronyms.filter(x => {
            if (x.acronym && searchTerm) {
                return (x.acronym.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
            }
        });
    }

    getAllAcronyms() {
        this.acronyms = allAcronyms;
        this.toggle();
    }

    sort() {
        this.acronyms.reverse();
    }

    toggle() {
        this.show = !this.show;
    }
}
