import { HistoryPage } from './components/history/history.page';
import { CompanyPage } from './components/company/company.page';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TwitterPage } from './components/twitter/twitter.page';
import { AcronymsPage } from './components/acronyms/acronyms.page';

@NgModule({
  declarations: [TwitterPage, AcronymsPage, CompanyPage, HistoryPage],
  imports: [IonicModule, CommonModule],
  providers: [
  ],
  exports: [
    TwitterPage, AcronymsPage, CompanyPage, HistoryPage
  ],
})
export class SharedModule { }
