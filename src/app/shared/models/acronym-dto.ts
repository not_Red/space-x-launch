// tslint:disable: variable-name

export class AcronymDTO {
    acronym: string;
    fullText: string;
    details: string;
}
