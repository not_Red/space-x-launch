// tslint:disable: variable-name
export class HistoryDTO {
    id: number;
    title: string;
    event_date_utc: string;
    event_date_unix: number;
    flight_number: number;
    details: string;
    links: {
      reddit: null,
      article: string;
      wikipedia: string;
    };
}
