import { EMPTY, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AlertController, LoadingController } from '@ionic/angular';
import { catchError, finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

    constructor(
        private readonly loadingCtrl: LoadingController,
        private readonly alertCtrl: AlertController,
        private readonly router: Router,
    ){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.loadingCtrl.getTop().then(hasLoading => {
            if (!hasLoading) {
                this.loadingCtrl.create({
                    spinner: 'bubbles',
                    message: 'Loading...',
                    translucent: true,
                    duration: 1000,
                }).then(loading => loading.present());
            }
        });

        return next.handle(request).pipe(
            catchError(err => {
                this.presentFailedAlert(err);
                return EMPTY;
            }),
            finalize(() => {
                this.loadingCtrl.getTop().then(hasLoading => {
                    if (hasLoading) {
                        this.loadingCtrl.dismiss();
                    }
                });
            })
        );
    }

    async presentFailedAlert(msg: any) {
        const alert = await this.alertCtrl.create({
            header: msg.status,
            message: msg.error,
            buttons: [{
                text: 'OK',
                handler: () => {
                    this.router.navigate(['']);
                }
              }]
        });
        await alert.present();
    }
}
