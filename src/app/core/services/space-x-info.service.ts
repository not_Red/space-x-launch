import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

const CONFIG = {
    SERVER_ADDR: 'https://api.spacexdata.com/v3',
  };

@Injectable({
  providedIn: 'root',
})
export class InfoService {

  constructor(
    private readonly http: HttpClient,
    ) { }

    public getHistory(): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/history`);
    }

    public getInfo(): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/info`);
    }

    public getAcronyms(): Observable<any> {
      return this.http.get<any>(`http://decronym.xyz/acronyms/SpaceX.json`);
  }

}
