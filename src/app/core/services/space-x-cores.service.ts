import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

const CONFIG = {
    SERVER_ADDR: 'https://api.spacexdata.com/v3/cores',
  };

@Injectable({
  providedIn: 'root',
})
export class CoresService {

  constructor(
    private readonly http: HttpClient,
    ) { }


      public getAllCores(limit: number, offset: number): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}?limit=${limit}&offset=${offset}`);
      }

      public getSingleCore(id: string): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/${id}`);
      }

      public searchCore(param: string): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}?core_serial=${param}`);
      }
}
