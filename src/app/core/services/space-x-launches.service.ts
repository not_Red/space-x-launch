import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

const CONFIG = {
    SERVER_ADDR: 'https://api.spacexdata.com/v3',
  };



@Injectable({
  providedIn: 'root',
})
export class LaunchesService {

  constructor(
    private readonly http: HttpClient,
    ) { }
  public getAllLaunches(): Observable<any> {
    return this.http.get<any>(`${CONFIG.SERVER_ADDR}/launches`);
  }

  public getPastLaunches(limit: number, offset: number): Observable<any> {
    return this.http.get<any>(`${CONFIG.SERVER_ADDR}/launches/past?order=desc&limit=${limit}&offset=${offset}`);
  }

  public getLatestLaunches(): Observable<any> {
    return this.http.get<any>(`${CONFIG.SERVER_ADDR}/launches/latest`);
  }

  public getNextLaunches(): Observable<any> {
    return this.http.get<any>(`${CONFIG.SERVER_ADDR}/launches/next`);
  }

  public getUpcomingLaunches(limit: number, offset: number): Observable<any> {
    return this.http.get<any>(`${CONFIG.SERVER_ADDR}/launches/upcoming?limit=${limit}&offset=${offset}`);
  }

  public getSingleLaunch(id: string): Observable<any> {
    return this.http.get<any>(`${CONFIG.SERVER_ADDR}/launches/${id}`);
  }
}
