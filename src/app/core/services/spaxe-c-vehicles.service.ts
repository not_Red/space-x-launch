import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

const CONFIG = {
    SERVER_ADDR: 'https://api.spacexdata.com/v3',
  };

@Injectable({
  providedIn: 'root',
})
export class VehiclesService {

  constructor(
    private readonly http: HttpClient,
    ) { }

    public getAllCapsules(): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/capsules`);
      }

    public getAllRockets(): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/rockets`);
      }

      public getSingleRocket(id: string): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/rockets/${id}`);
      }
      public getSingleCapsule(id: string): Observable<any> {
        return this.http.get<any>(`${CONFIG.SERVER_ADDR}/capsules/${id}`);
      }
}
