import { SingleLaunchPage } from './single-launch/single-launch.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LaunchesPage } from './launches.page';

const routes: Routes = [
  {
      path: '',
      component: LaunchesPage,
  },
  {
      path: ':id',
      component: SingleLaunchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LaunchesPageRoutingModule {}
