import { LaunchDTO } from './../../shared/models/launch-dto';
import { Component, OnInit, ViewChild } from '@angular/core';

import { LaunchesService } from 'src/app/core/services/space-x-launches.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-upcoming-launches',
  templateUrl: 'upcoming-launches.page.html',
  styleUrls: ['upcoming-launches.page.css']
})
export class UpcomingLaunchesPage implements OnInit {
  launches: LaunchDTO[];
  offset = 0;
  limit = 7;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(
    private readonly launchesService: LaunchesService,
    private readonly router: Router,

  ) {}

  ngOnInit() {
    this.launchesService.getUpcomingLaunches(this.limit, this.offset).subscribe((data) => {
        this.launches = data;
      });
  }

  loadData(event?) {
    this.limit += 5;
    this.offset += 5;
    this.launchesService.getUpcomingLaunches(this.limit, this.offset).subscribe((data) => {
      this.launches = this.launches.concat(data);
    });

    if (event) {
      event.target.complete();
    }
  }

  launchPage(launch: LaunchDTO): void {
    this.router.navigate(['/launches', launch.flight_number]);
  }

}
