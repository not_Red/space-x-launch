import { UpcomingLaunchesPage } from './upcoming-launches/upcoming-launches.page';
import { SingleLaunchPage } from './single-launch/single-launch.page';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LaunchesPage } from './launches.page';

import {LaunchesPageRoutingModule } from './launches-routing.module';
import { PastLaunchesPage } from './past-launches/past-launches.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    LaunchesPageRoutingModule
  ],
  declarations: [LaunchesPage, SingleLaunchPage, PastLaunchesPage, UpcomingLaunchesPage ]
})
export class LaunchesPageModule {}
