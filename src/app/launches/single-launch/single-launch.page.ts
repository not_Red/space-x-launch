import { LaunchDTO } from './../../shared/models/launch-dto';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LaunchesService } from 'src/app/core/services/space-x-launches.service';
import { Location, NumberSymbol } from '@angular/common';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-single-launch',
  templateUrl: 'single-launch.page.html',
  styleUrls: ['single-launch.page.css']
})
export class SingleLaunchPage implements OnInit {
  launchId: string;
  launch: LaunchDTO;
  constructor(
    private readonly launchesService: LaunchesService,
    private readonly route: ActivatedRoute,
    public readonly toastController: ToastController,
    private readonly location: Location,
    private readonly router: Router,

  ) {}

  ngOnInit() {
    this.launchId = this.route.snapshot.paramMap.get('id');
    this.launchesService.getSingleLaunch(this.launchId).subscribe((data) => {
      this.launch = data;
    });
  }
  goBack() {
    this.location.back();
  }

  async toastTest() {
    const toast = await this.toastController.create({
      message: 'Work in progress!',
      duration: 2000,
    });
    toast.present();
  }

  rocketPage(rocketId: string): void {
    this.router.navigate(['/vehicles/rockets', rocketId]);
  }
}
