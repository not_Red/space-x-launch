import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-launches',
  templateUrl: 'launches.page.html',
  styleUrls: ['launches.page.css']
})
export class LaunchesPage implements OnInit {

  upcomingLaunches = true;
  public show = false;

  constructor(

  ) {}
  ngOnInit() {

  }

  filterList() {}

  loadUpcoming() {
    this.upcomingLaunches = true;
  }
  loadPast() {
    this.upcomingLaunches = false;
  }
  sort() {}
  toggle() {
    this.show = !this.show;
  }
}
