import { LaunchDTO } from './../../shared/models/launch-dto';
import { Component, OnInit, ViewChild } from '@angular/core';

import { LaunchesService } from 'src/app/core/services/space-x-launches.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-past-launches',
  templateUrl: 'past-launches.page.html',
  styleUrls: ['past-launches.page.css']
})
export class PastLaunchesPage implements OnInit {
  launches: LaunchDTO[];
  offset = 0;
  limit = 7;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(
    private readonly launchesService: LaunchesService,
    private readonly router: Router,

  ) {}

  ngOnInit() {
    this.launchesService.getPastLaunches(this.limit, this.offset).subscribe((data) => {
        this.launches = data;
      });
  }

  loadData(event?) {
    this.limit += 5;
    this.offset += 5;
    this.launchesService.getPastLaunches(this.limit, this.offset).subscribe((data) => {
      this.launches = this.launches.concat(data);
    });

    if (event) {
      event.target.complete();
    }
  }

  launchPage(launch: LaunchDTO): void {
    this.router.navigate(['/launches', launch.flight_number]);
  }

}
