import { CoreDTO } from './../shared/models/core-dto';
import { CoresService } from './../core/services/space-x-cores.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-cores',
  templateUrl: 'cores.page.html',
  styleUrls: ['cores.page.css']
})
export class CoresPage implements OnInit {
  cores: CoreDTO[];
  offset = 0;
  limit = 5;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  public show = false;
  constructor(
    private readonly coresService: CoresService,

  ) {}

  ngOnInit() {
    this.coresService.getAllCores(this.limit, this.offset).subscribe((data) => {
      this.cores = data;
    });
  }

  loadData(event?) {
    this.limit += 5;
    this.offset += 5;
    this.coresService.getAllCores(this.limit, this.offset).subscribe((data) => {
      this.cores = this.cores.concat(data);
    });

    if (event) {
      event.target.complete();
    }
  }

  filterList(evt) {
    const searchTerm = evt.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.coresService.searchCore(searchTerm).subscribe((data) => {
      this.cores = data;
      });
  }

  getCores() {
    this.limit = 5;
    this.offset = 0;
    this.coresService.getAllCores(this.limit, this.offset).subscribe((data) => {
      this.cores = data;
    });
    this.toggle();
  }

  sort() {}

  toggle() {
    this.show = !this.show;
  }

}
