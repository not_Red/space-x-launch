import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CoresPage } from './cores.page';

import { CoresPageRoutingModule } from './cores-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    CoresPageRoutingModule,
  ],
  declarations: [CoresPage]
})
export class CoresPageModule {}
